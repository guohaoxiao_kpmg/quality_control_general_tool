echo "Please type input,output,count,delimitor"

[ "$#" -eq 4 ] || exit "4 argument required, $# provided"

input=$1
output=$2
count=$3
delimitor=$4


hadoop dfs -rmr $output
rm configure
echo "count:"$count > configure
echo "delimitor:"$delimitor >> configure


hadoop jar /usr/hdp/current/hadoop-mapreduce-client/hadoop-streaming.jar  -Dmapred.reduce.tasks=16 -file mapper.py -mapper "python mapper.py" -file reducer.py  -reducer "python reducer.py" -input $input -file configure -output $output

hadoop dfs -cat $output"/part*"
