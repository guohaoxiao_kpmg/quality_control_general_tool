#!/usr/bin/env python
import sys
count=0
delimitor="|"
for line in open("configure") :
    if "count" in line:
        count=int(line.rstrip("\n").split(":")[1])
    if "delimitor" in line:
        delimitor=line.rstrip("\n").split(":")[1]

# input comes from STDIN (standard input)
for line in sys.stdin:
    # remove leading and trailing whitespace
    line = line.strip()
    # split the line into words
    words = line.split(delimitor)
    # increase counters
    if len(words) != count:
         print '%s\t%s' % (len(words), 1)
