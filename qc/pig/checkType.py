#!/usr/bin/python 

# explicitly import Pig class 
from org.apache.pig.scripting import Pig 
import sys

    
class ExpectedType:
    type=""
    column=""
    def __str__(self):
        return self.column+":"+self.type

class TypeCheck:

    columns=""
    expectedType=[]
    def readTableFormat(self, path):
        f=open(path,'r')
        out = f.readlines() # will append in the list out
        for line in out:
            if "#" not in line:
                if len(line.split(" ")) > 1:
                    self.columns=self.columns+","+line.split(" ")[0]
        self.columns=self.columns[1:]
        print self.columns

    def readConfiguration(self, path):
        f=open(path,'r')
        out = f.readlines() # will append in the list out
        for line in out:
            if "#" not in line:
                if len(line.split(":")) > 1:
                    line=line.replace("\n","")
                    temp=ExpectedType()
                    temp.type=line.split(":")[1]
                    temp.column=line.split(":")[0]
                    self.expectedType.append(temp)
        for element in self.expectedType:
            print element
 
    def check(self):
        generate_statement="X= FOREACH a GENERATE"
        dump_statement="LOG_COUNT = FOREACH (GROUP X ALL) GENERATE COUNT(X) as total," 
        for element in self.expectedType:
            generate_statement=generate_statement+"(( "+ element.type+") "+element.column+" is null?0:1) as  "+element.column+"f,"
            dump_statement=dump_statement+"SUM( X."+element.column+"f) as " +element.column+","
        generate_statement=generate_statement[:-1]
        dump_statement= dump_statement[:-1]
        P = Pig.compile("a = load '$in'  USING PigStorage('"+self.delimitor+"') as ("+typeChecker.columns+");"+generate_statement+";"+ dump_statement+";dump LOG_COUNT;")


        # BIND and RUN 
        self.result = P.bind({'in':self.input, 'out':self.output}).runSingle()

        if self.result.isSuccessful() :
            print 'Pig job succeeded'
        else :
            raise 'Pig job failed'
    def summary(self):
        name="total"
        for element in self.expectedType:
            name=name+","+element.column
        print name
        print str(self.result.result("LOG_COUNT").iterator().next())
    
    def setInputOutput(self,input,output,delimitor):
        self.input=input
        self.output=output
        self.delimitor=delimitor




if __name__ == "__main__":
    print sys.argv
    if len(sys.argv) < 6:
        print "you should input table foramt, configure, input path, output path,delimitor"
        sys.exit(0)
    typeChecker = TypeCheck()
    typeChecker.setInputOutput(sys.argv[3],sys.argv[4],sys.argv[5])
    typeChecker.readTableFormat(sys.argv[1])
    typeChecker.readConfiguration(sys.argv[2])
    typeChecker.check()
    typeChecker.summary()
    # COMPILE: compile method returns a Pig object that represents the pipeline
    
